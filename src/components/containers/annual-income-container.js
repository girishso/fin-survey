import React, { Component } from 'react';
import { connect } from 'react-redux';
import AnnualIncome from '../views/annual-income';


class AnnualIncomeContainer extends Component {

    render() {
        return (
          <AnnualIncome initialValues={{annual_income: 0, household_income:  10}} />
        );
    }

}

const mapStateToProps = function(store) {
  return {
    details: store.annual_income
  };
};

export default connect(mapStateToProps)(AnnualIncomeContainer);
