import React, { Component } from 'react';
import { connect } from 'react-redux';
import NetWorth from '../views/net-worth';


class NetWorthContainer extends Component {
  render() {
      return (
        <NetWorth />
      );
  }
}

export default connect(mapStateToProps)(NetWorthContainer);
