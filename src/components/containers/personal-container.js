import React, { Component } from 'react';
import { connect } from 'react-redux';
import Personal from '../views/personal';

class PersonalContainer extends Component {

  render() {
    return (
      <Personal details={this.props.details} />
    );
  }

}

const mapStateToProps = function(store) {
  return {
    details: store.personalState
  };
};

export default connect(mapStateToProps)(PersonalContainer);
