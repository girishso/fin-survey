import React from 'react';

const Home = React.createClass({
  render: function() {
    return (
      <div className="home-page">
        <h1>demo Redux</h1>
      </div>
    );
  }
});

export default Home;
