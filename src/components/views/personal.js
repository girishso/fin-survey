import React, { Component } from 'react';
import { Container, Step, Accordion, Icon, Grid, Dropdown, Button, Checkbox, Form, Divider, List} from 'semantic-ui-react'
import Qs from '../../questions-matrix.json'
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import { reduxForm, Field } from 'redux-form'
import renderField, {renderSelector, renderDatePicker} from './utils/render-field'
import moment from 'moment';
import validate from './validate'
var {Link, NamedURLResolver} = require("react-router-named-routes");

const marital_statuses = ["", "Married", "Unmarried"]

const employment_statuses = ["", "Employed", "Homemaker", "Retired", "Self-employed", "Student", "Unemployed"]

const Personal = props => {
    const {handleSubmit, pristine, previousPage, submitting} = props
    return (
        <div>
            <h1>Personal circumstances</h1>
            <p className="summary">
                <strong>Eric</strong>, please complete your personal information.
                <br/>
                Anim duis velit culpa qui laboris cillum enim veniam duis occaecat. In ullamco consectetur nulla tempor dolor labore consectetur est labore deserunt do cupidatat in minim aute.
            </p>
            <Accordion>
              <Accordion.Title>
                <Icon name="angle down" />
              </Accordion.Title>
              <Accordion.Content>
                Officia ut pariatur amet incididunt veniam ex ullamco in pariatur occaecat quis anim nulla. Et dolor commodo laborum reprehenderit ad mollit in enim labore occaecat anim nisi. Culpa reprehenderit elit sed non mollit dolore ad aliqua duis.
              </Accordion.Content>
            </Accordion>
            <Divider />
            <Form onSubmit={handleSubmit}>
              <Field
                name="dob"
                component={renderDatePicker}
                label="Date of birth"
              />
              <Field
                name="n_dependents"
                type="number"
                component={renderField}
                label="Number of dependents"
              />
              <Field
                name="marital_status"
                component={renderSelector}
                options={marital_statuses}
                label="Marital status"
              />
              <Field
                name="employment_status"
                component={renderSelector}
                options={employment_statuses}
                label="Employment status"
              />
              <Divider hidden />
              <Grid columns='equal'>
                <Grid.Column>
                </Grid.Column>

                <Grid.Column>
                  <Button type='submit' className='positive fluid big'>Next</Button>
                </Grid.Column>
              </Grid>
        	</Form>

        </div>)
}

const handleChange = x => console.log(x)

export default reduxForm({
  form: 'wizard', // <------ same form name
  destroyOnUnmount: false, // <------ preserve form data
  forceUnregisterOnUnmount: true, // <------ unregister fields on unmount
  validate
  }
)(Personal)
