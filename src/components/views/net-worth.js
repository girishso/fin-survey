import React from 'react';
// import { Link } from 'react-router';
import { Header, Icon, Button, Message, Input, Segment, Label, Accordion, Grid, List, Checkbox, Form, Divider} from 'semantic-ui-react'
import { reduxForm, Field, formValueSelector } from 'redux-form'
import renderField, {renderLabeledField} from './utils/render-field'
import validate from './validate'
import RenderWizardButtons from './utils/render-wizard-btns'
import { connect } from 'react-redux';
var {Link, NamedURLResolver} = require("react-router-named-routes");


const NetWorth = props => {
  const {handleSubmit, pristine, previousPage, submitting, error} = props
  return (
    <div>
        <h1>Net worth</h1>
        <p className="summary">
            <strong>Eric</strong>, is your new worth correct?
            <br/>
            <br/>
            Ex sit sunt id eiusmod nostrud ea esse nulla eu exercitation adipisicing ad aute. Pariatur ad culpa sint dolor est labore quis dolore aute anim nostrud.
        </p>
        <Accordion>
          <Accordion.Title>
            <Icon name="angle down" />
          </Accordion.Title>
          <Accordion.Content>
            Sint consequat elit reprehenderit dolor culpa proident dolor qui occaecat ut exercitation fugiat magna in officia anim. Pariatur ullamco laboris irure excepteur aute pariatur qui magna occaecat velit in velit proident esse sint minim non. Dolore occaecat non laborum qui dolore consequat excepteur amet amet.
          </Accordion.Content>
        </Accordion>
        <Divider />
          <Form onSubmit={handleSubmit}>
              <Field
                name="estimated_total_assets"
                type="number"
                component={renderLabeledField}
                attached_label = "$"
                label="Estimated total assets"
              />
              <div className='three fields'>
                <Field
                  name="liquid_assets"
                  type="number"
                  component={renderLabeledField}
                  attached_label = "$"
                  disabled={true}
                  label="Liquid assets"
                />

                <div className='field'>
                  <label>&nbsp;</label>
                  <div>
                    <input className='fluid' type="range" />
                  </div>
                </div>

                <Field
                  name="fixed_assets"
                  type="number"
                  component={renderLabeledField}
                  attached_label = "$"
                  label="Fixed assets"
                />
              </div>

              <Field
                name="estimated_total_liabilities"
                type="number"
                component={renderLabeledField}
                attached_label = "$"
                label="Estimated total liabilities"
              />
          <div className="ui segment grey">
            <div className="ui grid">
              <div className="left floated eight wide column">
                <label className="bold">Estimated total net worth</label>
              </div>
              <div className="right floated four wide column right aligned"><span className="bold">{props.total_net_worth} $</span></div>
            </div>
          </div>
          <RenderWizardButtons {...props} />
          </Form>

    </div>

  )
}
const mapStateToProps = (state) => {
  let total = 0
  if(state.form.wizard.fields.estimated_total_assets && state.form.wizard.fields.estimated_total_liabilities) {
    total = (+state.form.wizard.values.estimated_total_assets || 0) - (+state.form.wizard.values.estimated_total_liabilities || 0)
  }

  return {
    total_net_worth: thousands_sep(total)
  }
};

const thousands_sep = (x) => {
  var parts = x.toString().split(".");
  parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  return parts.join(".");
}

export default reduxForm({
  form: 'wizard',
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: true,
  validate,
  })(connect(
    mapStateToProps
)(NetWorth))

