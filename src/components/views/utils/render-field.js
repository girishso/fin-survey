import React from 'react'
import DatePicker from 'react-datepicker';
import moment from 'moment';

const renderField = ({input, label, type, meta: {touched, error}}) => (
  <div className='field'>
    <label>{label}</label>
    <div>
      <input {...input} placeholder={label} type={type} />
      {touched && error && <span>{error}</span>}
    </div>
  </div>
)

export const renderSelector = ({input, label, options, meta: {touched, error}}) => (
  <div className='field'>
    <label>{label}</label>
    <div className=''>
      <select className='ui dropdown' {...input}>
        {options.map(val => <option className='item' value={val} key={val}>{val}</option>)}
      </select>
      {touched && error && <span>{error}</span>}
    </div>
  </div>
)

export const renderDatePicker = ({input, label, placeholder, defaultValue, meta: {touched, error} }) => (
  <div className='field'>
    <label>{label}</label>
    <DatePicker {...input} dateForm="MM/DD/YYYY"
      selected={input.value ? moment(input.value) : null}
      showMonthDropdown
      showYearDropdown
      dropdownMode="select"
      dateFormatCalendar="MMMM"
     />
    {touched && error && <span className='error'>{error}</span>}
  </div>
);

export const renderLabeledField = ({input, label, attached_label, type, meta: {touched, error}}) => (
  <div className="field">
    <label>{label}</label>
    <div className="ui right labeled input small">
      <input {...input} placeholder={label} type={type} className="ta-right" />
      <div className="ui label">{attached_label}</div>
    </div>
    {touched && error && <span className='error'>{error}</span>}
  </div>
)

export default renderField
