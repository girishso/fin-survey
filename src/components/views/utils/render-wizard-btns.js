import React from 'react'
import { Header, Icon, Button, Message, Input, Segment, Label, Accordion, Grid, List, Checkbox, Form, Divider} from 'semantic-ui-react'

const RenderWizardButtons = props => (
  <div>
    <Divider hidden />
    <Grid columns='equal'>
        <Grid.Column>
          <Button className='fluid big' onClick={props.previousPage}>Previous</Button>
        </Grid.Column>
        <Grid.Column>
          <Button type='submit'   className='positive fluid big'>Next</Button>
        </Grid.Column>
    </Grid>
  </div>
)
export const Home = () => <h1>Home</h1>

export default RenderWizardButtons

// disabled={props.pristine || props.submitting}
