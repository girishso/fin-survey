import React from 'react';

// Using "Stateless Functional Components"
export default function(props) {
    if(typeof props.url === 'undefined')
        return (<h1>Loading...</h1>)


    const link_text = (props.h1 !== null && props.h1.length > 0) ? props.h1[0] : props.url;
    const h1s = props.h1 !== null && props.h1.map((h1, i) => <p key={i}>{h1}</p>)
    const h2s = props.h2 !== null && props.h2.map((h2, i) => <p key={i}>{h2}</p>)
    const h3s = props.h3 !== null && props.h3.map((h3, i) => <p key={i}>{h3}</p>)
    return (
        <div>
            <a href={props.url}><h1>{link_text}</h1></a>
            <h1>h1</h1>
            <div> {h1s} </div>
            <h1>h2</h1>
            <div> {h2s} </div>
            <h1>h3</h1>
            <div> {h3s} </div>
        </div>
    );
}
