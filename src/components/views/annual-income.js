import React from 'react';
// import { Link } from 'react-router';
import { Header, Icon, Button, Message, Input, Segment, Label, Accordion, Grid, List, Checkbox, Form, Divider} from 'semantic-ui-react'
import { reduxForm, Field, formValueSelector } from 'redux-form'
import renderField, {renderLabeledField} from './utils/render-field'
import validate from './validate'
import { connect } from 'react-redux';
import RenderWizardButtons from './utils/render-wizard-btns'

var {Link, NamedURLResolver} = require("react-router-named-routes");


const AnnualIncome = props => {
  const {handleSubmit, pristine, previousPage, submitting, error} = props
  return (
    <div>
        <h1>Annual individual income</h1>
        <p className="summary">
            Et dolor sit ullamco consequat in non exercitation dolor velit non tempor ut eiusmod pariatur ea adipisicing irure proident.
            <br/>
            <br/>
            Lorem ipsum laborum cupidatat eu dolore quis do non pariatur esse aliqua. Non est nulla fugiat sint velit anim ut in. Nulla minim nisi laboris do do in ut fugiat fugiat velit incididunt consectetur labore ea sit esse officia.
        </p>
        <Accordion>
          <Accordion.Title>
            <Icon name="angle down" />
          </Accordion.Title>
          <Accordion.Content>
            Dolore adipisicing est sunt ea nulla esse officia nostrud deserunt aliquip enim dolor qui dolore elit sunt proident. Ex cupidatat nulla pariatur cillum consectetur mollit ut velit qui non in proident do. Aliquip enim proident quis in nulla sint ut adipisicing qui minim nisi est nulla nisi irure culpa fugiat. Enim amet elit eu ullamco ad ullamco cillum in mollit ut fugiat cupidatat ut reprehenderit dolor duis. Lorem ipsum sunt mollit incididunt consequat consequat sunt esse veniam minim.
          </Accordion.Content>
        </Accordion>
        <Divider />
          <Form onSubmit={handleSubmit}>
              <Field
                name="annual_income"
                type="number"
                component={renderLabeledField}
                attached_label = "$/Yr"
                label="Annual individual income"
              />
              <Field
                name="household_income"
                type="number"
                component={renderLabeledField}
                attached_label = "$/Yr"
                label="Additional household income"
              />
          <div className="ui segment grey">
            <div className="ui grid">
              <div className="left floated nine wide column">
                <label className="bold">Estimated total annual income</label>
              </div>
              <div className="right floated three wide column right aligned"><span className="bold">{props.total} $/Yr</span></div>
            </div>
          </div>
          <RenderWizardButtons {...props} />
          </Form>

    </div>

  )
}
const mapStateToProps = (state) => {
  let total = 0
  // if(state.form.wizard.fields.annual_income && state.form.wizard.fields.household_income) {
    total = (+state.form.wizard.values.annual_income || 0) + (+state.form.wizard.values.household_income || 0)
  // }

  return {
    total: thousands_sep(total)
  }
};

const thousands_sep = (x) => {
  var parts = x.toString().split(".");
  parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  return parts.join(".");
}

export default reduxForm({
  form: 'wizard',
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: true,
  validate,
  })(connect(
    mapStateToProps
)(AnnualIncome))

