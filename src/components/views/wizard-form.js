import React, {Component} from 'react'
import PropTypes from 'prop-types'
import Personal from './personal'
import AnnualIncome from './annual-income'
import NetWorth from './net-worth'
import { Container, Step, Accordion, Icon, Button, Checkbox, Form, Divider} from 'semantic-ui-react'
import { resolve } from "react-router-named-routes";

class WizardForm extends Component {
  constructor(props) {
    super(props)
    this.nextPage = this.nextPage.bind(this)
    this.previousPage = this.previousPage.bind(this)
    this.state = {
      page: 1
    }
  }
  nextPage() {
    this.setState({page: this.state.page + 1})
  }

  previousPage() {
    this.setState({page: this.state.page - 1})
  }

  render() {
    const {page} = this.state
    const steps = [
      { title: 'Personal circumstances', page: 1 },
      { title: 'Annual individual income', page: 2 },
      { title: 'Net worth', page: 3 }
    ];

    const new_steps = steps.map(x => {
      if(x.page == page) {
        return {title: x.title, icon: 'circle thin', active: true}
      } else {
        return {title: x.title, icon: 'circle', active: false}
      }
    })

    const {onSubmit} = this.props
    return (
      <Container className="doubling stackable grid">
        <div className="row main">
          <div className="four wide column">
            <Step.Group vertical size="mini" items={new_steps} className="" />
          </div>
          <div className="twelve wide column">
            <div className="ui segment raised">
              {page === 1 &&
                <Personal onSubmit={this.nextPage}
                initialValues={{
                  n_dependents: 0,
                  annual_income: 0, household_income:  0,
                  estimated_total_assets: 0, estimated_total_liabilities: 0,
                }}
                 />}
              {page === 2 &&
                <AnnualIncome
                  previousPage={this.previousPage}
                  onSubmit={this.nextPage}
                />}
              {page === 3 &&
                <NetWorth
                  previousPage={this.previousPage}
                  onSubmit={this.nextPage}
                />}
            </div>
          </div>

        </div>
      </Container>

    )
  }
}

WizardForm.propTypes = {
  onSubmit: PropTypes.func.isRequired
}

export default WizardForm
