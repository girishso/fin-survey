import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import { Provider } from 'react-redux';
import router from './router';
import store from './store'
import './index.css';
var {Link, NamedURLResolver} = require("react-router-named-routes");
NamedURLResolver.mergeRouteTree(router, "/");

ReactDOM.render(<Provider store={store}>{router}</Provider>, document.getElementById('root'));
registerServiceWorker();
