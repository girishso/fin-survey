import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form'

// Reducers
import personalReducer from './personal-reducer'

// Combine Reducers
var reducers = combineReducers({
    // personalState: personalReducer,
    form: formReducer,
});

export default reducers;
