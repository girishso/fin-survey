import React, { Component } from 'react';
import { Container, Step, Accordion, Icon, Button, Checkbox, Form, Divider} from 'semantic-ui-react'
import './App.css'
import Qs from './questions-matrix.json'

class App extends Component {
  render() {
    console.log(Qs)
    const steps = [
      { completed: true, icon: 'truck', title: 'Personal circumstances' },
      { completed: true, icon: 'credit card', title: 'Annual individual income' },
      { active: true, icon: 'info', title: 'Your recommended portfolio' },
    ];

    return (

      <Container className="doubling stackable grid">
        <div className="row main">
          <div className="four wide column">
            <Step.Group vertical size="mini" items={steps} className="" />
          </div>
          <div className="twelve wide column">
            <div className="ui segment raised">
              <div className="ui header">Holdings</div>
              <p>Lorem ipsum</p>
              <Accordion>
                <Accordion.Title>
                  <Icon name="angle down" />
                </Accordion.Title>
                <Accordion.Content>
                  $42
                </Accordion.Content>
              </Accordion>
              <Divider />
              <Form>
                <Form.Field>
                  <label>First Name</label>
                  <input placeholder='First Name' />
                </Form.Field>
                <Form.Field>
                  <label>Last Name</label>
                  <input placeholder='Last Name' />
                </Form.Field>
                <Form.Field>
                  <Checkbox label='I agree to the Terms and Conditions' />
                </Form.Field>
                <Button type='submit'>Submit</Button>
              </Form>
            </div>
          </div>

        </div>
      </Container>
    );
  }
}

export default App;
