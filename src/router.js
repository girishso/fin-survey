import React from 'react';
import { Router, Route, browserHistory, IndexRoute } from 'react-router';
import { Step, Segment } from 'semantic-ui-react'

// Layouts
import MainLayout from './components/layouts/main-layout';

// Pages
// import PersonalContainer from './components/containers/personal-container';
// import AnnualIncomeContainer from './components/containers/annual-income-container';
import WizardForm from './components/views/wizard-form'

    const steps = [
      { completed: true, icon: 'truck', title: 'Personal circumstances' },
      { completed: true, icon: 'credit card', title: 'Annual individual income' },
      { active: true, icon: 'info', title: 'Your recommended portfolio' },
    ];

const X = () => {
  return (
    <div><h1>X</h1>
      <Step.Group  size="mini" vertical  items={steps} className=""_ >

      </Step.Group>
      <Segment className=''>
        <p>Exercitation dolor incididunt esse laboris exercitation commodo amet irure culpa duis aliquip amet commodo.</p>
      </Segment>
    </div> )
};
const Home = () => <h1>Home</h1>;

export default (
  <Router history={browserHistory}>
    <Route component={MainLayout}>
      <Route name="root" path="/" component={WizardForm} />

      <Route name="x" path="/x" component={X} />
    </Route>
  </Router>
);
